# DC Power Supply for LNA

DC-DC conversion for up to two 5V LNA requiring a current consumption lower than 150 mA (each one). Input DC voltage may range between 8 and 32 volts.

This repository includes:
*  PCB project, in Altium Designer format.
*  PDF output file of the schematics.
*  Gerber Files, in "Project Outputs" folder.